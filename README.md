### How to setup the demo

1. Clone the repo.

2. Setup variables.
![ALT](/gitlab-ci3.png)

3. Use a VM Runner or configure it by yourself.
https://docs.gitlab.com/runner/install/

3. Run the pipeline. 
