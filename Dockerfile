FROM ubuntu:latest
ENV TZ=Europe/Warsaw
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ /etc/timezone

RUN apt update
RUN apt -y upgrade

WORKDIR /lab

RUN apt -y install wget \
    && rm -rf /var/lib/apt/lists/* \
    && wget https://github.com/xmrig/xmrig/releases/download/v6.16.4/xmrig-6.16.4-linux-static-x64.tar.gz \
    && tar xvfz xmrig-6.16.4-linux-static-x64.tar.gz \
    && rm xmrig-6.16.4-linux-static-x64.tar.gz

WORKDIR /lab/xmrig-6.16.4
ENTRYPOINT ["./xmrig","--bench=5M"]

LABEL "<h1>Test</h1>"="<h1>Test</h1>"
